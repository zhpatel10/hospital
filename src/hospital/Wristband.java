/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital;

import java.util.Date;

/**
 *
 * @author Zalak Patel
 */
public class Wristband {
       protected String name;
       protected Doctor doc;
       protected Date dob;
       protected Barcode barc;

    public Wristband(String name, Doctor doc, Date dob,Barcode barc) {
        this.name = name;
        this.doc = doc;
        this.dob = dob;
        this.barc=barc;
    }

    public String getName() {
        return name;
    }

    public Doctor getDoc() {
        return doc;
    }

    public Date getDob() {
        return dob;
    }

    public Barcode getBarc() {
        return barc;
    }
        
        
}
